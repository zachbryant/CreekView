package burntapples.github.io.creekview;

import org.apache.commons.lang.WordUtils;

public class Assignment implements Comparable{
    private String grade, type, dateAssigned, dateDue, name;

    public Assignment(String n, String g, String t, String da, String dd){
        name= WordUtils.capitalize(n.trim());
		try {
			grade = "" + Double.valueOf(g.trim());
			//grade=grade.substring(0,grade.indexOf("."));
		}catch(Exception e){ grade="Not Graded";}
        type=WordUtils.capitalize(t.trim());
        dateAssigned=da.trim();
        dateDue=dd.trim();
    }
    public String getDateDue(){
        return dateDue;
    }
    public String getDateAssigned(){
        return dateAssigned;
    }
    public String getType(){
        return type;
    }
    public String getName(){
        return name;
    }
    public String getGrade(){
        return grade;
    }
    public String getFormattedDate(){
        if(dateDue.length()==0){
            return "Assigned on "+dateAssigned;
        }
        if(dateAssigned.length()==0){
            return dateDue;
        }
        if(dateAssigned.equals(dateDue)){
            return dateDue;
        }
        return String.format("%s - %s",dateAssigned,dateDue);
    }
    public String toString(){
        return String.format("%s %s %s",dateDue,name,grade);
    }
    public int compareTo(Object o){
        String[] dateDueList = dateDue.split("/");
        String[] oList = ((Assignment)o).getDateDue().split("/");
        if(dateDueList[2]==oList[2]){
            if(dateDueList[0]==oList[0]){
                return dateDueList[1].compareTo(oList[1]);
            }
            else{
                return dateDueList[0].compareTo(oList[0]);
            }
        }
        else{
            return dateDueList[2].compareTo(oList[2]);
        }
    }
}
