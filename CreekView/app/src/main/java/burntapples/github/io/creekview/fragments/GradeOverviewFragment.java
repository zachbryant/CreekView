package burntapples.github.io.creekview.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;

import burntapples.github.io.creekview.Assignment;
import burntapples.github.io.creekview.Group;
import burntapples.github.io.creekview.R;
import burntapples.github.io.creekview.Utils;
import burntapples.github.io.creekview.activities.ViewSwipeActivity;
import burntapples.github.io.creekview.adapter.ExpandableCardListAdapter;

public class GradeOverviewFragment extends Fragment {
	// more efficient than HashMap for mapping integers to objects
	SparseArray<Group> groups = new SparseArray<>();
	Utils utils = ViewSwipeActivity.utils;

	public GradeOverviewFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_grade_overview,container,false);
		createData();
		ExpandableListView listView = (ExpandableListView) rootView.findViewById(R.id.list_overView);
		ExpandableCardListAdapter adapter = new ExpandableCardListAdapter(inflater, groups);
		listView.setAdapter(adapter);
		return rootView;
	}
	public void createData() {
		ArrayList<ArrayList<Assignment>> rfList = utils.getOverviewCardDescriptions();
		Group future = new Group(String.format("%s%-4d","Recently Graded",rfList.get(0).size()));
		Group recent = new Group(String.format("%s|%-4d","Upcoming Assignments",rfList.get(1)
				.size()));
		future.children.addAll(rfList.get(0));
		recent.children.addAll(rfList.get(1));
		groups.append(0,future);
		groups.append(1,recent);
	}
}
