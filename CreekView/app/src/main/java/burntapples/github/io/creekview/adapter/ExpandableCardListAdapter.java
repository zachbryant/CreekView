package burntapples.github.io.creekview.adapter;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import burntapples.github.io.creekview.Assignment;
import burntapples.github.io.creekview.Group;
import burntapples.github.io.creekview.R;
import burntapples.github.io.creekview.Utils;

public class ExpandableCardListAdapter extends BaseExpandableListAdapter {

	private final SparseArray<Group> groups;
	public LayoutInflater inflater;

	public ExpandableCardListAdapter(LayoutInflater inflater, SparseArray<Group> groups) {
		this.groups = groups;
		this.inflater = inflater;
	}

	@Override
	public Assignment getChild(int groupPosition, int childPosition) {
		return groups.get(groupPosition).children.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		final Assignment child = getChild(groupPosition, childPosition);
		final ViewGroup vg_Parent =parent;
		TextView a_Name,a_Date,a_Grade,a_Type;
		ImageView gradeColor;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.card_layout, null);
		}
		a_Name = (TextView)convertView.findViewById(R.id.card_assignmentName);
		a_Date = (TextView)convertView.findViewById(R.id.card_date);
		a_Grade = (TextView)convertView.findViewById(R.id.card_grade);
		a_Type = (TextView)convertView.findViewById(R.id.card_grade_type);
		gradeColor = (ImageView)convertView.findViewById(R.id.grade_color);
		a_Name.setText(child.getName());
		a_Date.setText(child.getFormattedDate());
		a_Grade.setText(child.getGrade());
		a_Type.setText(child.getType());
		int c = Utils.getColor(child.getGrade());
		gradeColor.setBackgroundColor(c);

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//do stuff
			}
		});
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return groups.get(groupPosition).children.size();
	}

	@Override
	public Group getGroup(int groupPosition) {
		return groups.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return groups.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
							 View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.listrow_group, null);
		}
		Group group = getGroup(groupPosition);
		/*switch(group.string){
			case "Recently Graded"://set icon to calendar or something
				break;
			case "Upcoming Assignments":
		}*/
		((CheckedTextView) convertView).setText(group.string);
		((CheckedTextView) convertView).setChecked(isExpanded);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
}