package burntapples.github.io.creekview;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
    private byte[] key;
    private Key aesKey;
    private Cipher cipher;
    private HashMap<String, String> file_name = new HashMap<>();
    private HashMap<String, String> iv_file_name = new HashMap<>();
    private Context context;

    public AES(Context c){
        file_name.put("NAME", "butter.aes");
        file_name.put("STUDENT_ID", "milk.aes");
        file_name.put("STUDENT_PASSWORD", "eggs.aes");
        file_name.put("INIT_GRADE_PAGE", "flour.aes");
        file_name.put("REFRESH_TIME", "love.aes");
        file_name.put("INFO3", "cyanide.aes");

        iv_file_name.put("NAME", "churn.aes");
        iv_file_name.put("STUDENT_ID", "cow.aes");
        iv_file_name.put("STUDENT_PASSWORD", "chicken.aes");
        iv_file_name.put("INIT_GRADE_PAGE", "wheat.aes");
        iv_file_name.put("REFRESH_TIME", "hate.aes");
        iv_file_name.put("INFO3", "powder.aes");

        key = "j3472HAG6Lo20z10".getBytes();
        context = c;
        aesKey = new SecretKeySpec(key, "AES/CBC/PKCS5Padding");
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        }catch(Exception e){e.printStackTrace();}
    }
    protected AES(){
    }

    public final boolean jet(String place, String encrypt) {
        try {
            //init for encryption
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(encrypt.getBytes());
            byte[] iv = cipher.getIV();
            //store encrypted bytes and iv for later
            FileOutputStream writer = context.openFileOutput(file_name.get(place.toUpperCase()), Context.MODE_PRIVATE);
            FileOutputStream ivWriter = context.openFileOutput(iv_file_name.get(place.toUpperCase()), Context.MODE_PRIVATE);
            writer.write(encrypted);
            writer.close();
            ivWriter.write(iv);
            ivWriter.close();

            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public final String land(String place){

        String decrypted = null;
        try{
            // retrieve encrypted values and init vector
            FileInputStream reader = context.openFileInput(file_name.get(place.toUpperCase()));
            FileInputStream ivReader = context.openFileInput(iv_file_name.get(place.toUpperCase()));
            byte[] encrypted = new byte[reader.available()];
            byte[] iv = new byte[ivReader.available()];
            //populate arrays
            reader.read(encrypted);
            reader.close();
            ivReader.read(iv);
            ivReader.close();
            //init the cipher for decrypt mode
            IvParameterSpec IVPS = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, aesKey,IVPS);

            decrypted= new String(cipher.doFinal(encrypted));
        }catch(FileNotFoundException e){return null;}
        catch(IllegalBlockSizeException |
                BadPaddingException |
                InvalidKeyException |
                InvalidAlgorithmParameterException |
                IOException e){e.printStackTrace();}
        return decrypted;
    }

    public static void main(String[] args) {
    }
}