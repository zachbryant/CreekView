package burntapples.github.io.creekview.parser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class OverviewParser {

    //main method is for testing only

    public static void main(String[] args) {
    }

	//Returns the names of the staff involved in your classes, pass it a string which is either the url or the contents of the summary file,
	//depending on if you have marked the "isFilePathURL" tag as true.

	public final static ArrayList<String> getTeachers(String s,boolean isFilePathURL){
		ArrayList<String> ret = new ArrayList<>();
		try{
			String uri ="";
			if(isFilePathURL){
				URL res = new URL(s);
				Scanner dat = new Scanner(res.openStream());
				while(dat.hasNextLine()){
					String n=dat.nextLine();
					uri+=n+"\n";
				}
			}else uri=s;
			String[] work = uri.split("\n");
			for(int x=0;x<work.length;x++){
				if(work[x].contains("@cfisd.net")){
					String[] temp = work[x].split("\"");
					String name = temp[1].substring(temp[1].indexOf(":")+1,temp[1].indexOf("@"));
					if(!ret.contains(name))
						ret.add(name);
				}
			}
		}catch(Exception e){e.printStackTrace();}
		return ret;
	}

	//Pass either the URL or the contents, but distinguish with the isFilePathURL flag
	//with the class code as the key and the class data, assignments, average, and weights in the arraylist

	public final static HashMap<String,ArrayList<String>> getClasswork(String s,boolean isFilePathURL){
		HashMap<String,ArrayList<String>> classList = new HashMap<>();
		try{
			String uri ="";
			if(isFilePathURL){
				URL res = new URL(s);
				Scanner dat = new Scanner(res.openStream());
				while(dat.hasNextLine())
					uri+=dat.nextLine()+"\n";
			}else uri=s;
			String[] work = uri.split("\n");
			for(int x=0;x<work.length;x++)
				work[x]=work[x].trim();
			ArrayList<String> classes = filterClasses(work);
			ArrayList<String> cleaned = new ArrayList<>();
			for(int x=0;x<classes.size();x++)
				cleaned.add(classes.get(x).split(" ")[0]);
			boolean adding = false;
			int count=0;
			for(String e:work)
				if(e.equalsIgnoreCase("<div class=\"AssignmentClass\">"))
					count++;
			ArrayList<String> classwork = new ArrayList<>();
			String appendum="";
			for(int x=0;x<work.length-2;x++){
				if(work[x].equalsIgnoreCase("<div class=\"AssignmentClass\">")){
					adding=true;
				}
				if(work[x].equalsIgnoreCase("</div>")&&work[x+1].equalsIgnoreCase("</div>")&&work[x+2].equalsIgnoreCase("</div>")){
					adding=false;
				}
				if(adding){
					appendum+=(work[x].length()>1?work[x]+"\n":"");
				}else{
					classwork.add(appendum);
					appendum="";
				}
			}
			classList = getClassListings(classwork,cleaned);
		}catch(Exception e){e.printStackTrace();}
		return classList;
	}

	//Takes two arrays from getClasswork and cleans both of them up for final return, REQUIRED for getClasswork to work

	private static HashMap<String,ArrayList<String>> getClassListings(ArrayList<String>cl,ArrayList<String> cn){
		String publishedissues = "Published scores that are included in averages could not be found for this class for the selected report card run.";
		HashMap<String,String[]> ret = new HashMap<>();
		HashMap<String,ArrayList<String>> reto = new HashMap<>();
		HashSet<String> keys = new HashSet<>();
		for(String s:cn){
			for(String q:cl){
				if(q.contains(s)){
					ret.put(s, q.split("<tr class=\"sg-asp-table-data-row\">"));
					keys.add(s);
					break;
				}
			}
		}
		Iterator<String> ip = keys.iterator();
		while(ip.hasNext()){
			String key = ip.next();
			String[] listi = ret.get(key);
			for(int x=0;x<listi.length;x++)
				listi[x]=sweepTags(key,listi[x]);
			ret.put(key,listi);
		}
		ip=keys.iterator();
		while(ip.hasNext()){
			String key = ip.next();
			String[] pass = ret.get(key);
			reto.put(key,new ArrayList<>(Arrays.asList(pass)));
		}
		ip=keys.iterator();
		while(ip.hasNext()){
			String key = ip.next();
			ArrayList<String> oppai = reto.get(key);
			ArrayList<String> oppa = new ArrayList<>();
			for(String s:oppai)
				if(s.contains("@")){
					oppa.add(s.substring(0,s.indexOf("@")));
					oppa.add(s.substring(s.indexOf("@")+1));
				}else if(!s.contains(publishedissues))
					oppa.add(s);
			oppai = new ArrayList<>(oppa);
			oppa = new ArrayList<>();
			reto.put(key,oppai);
			for(String s:oppai){
				String retopotato = "";
				String[] pot = s.split("_");
				HashSet<Integer> spots = new HashSet<>();
				for(int x=0;x<pot.length;x++)
					if(pot[x].contains("."))
						spots.add(x);
				if(pot[pot.length-1].contains("%"))
					spots.remove(pot.length-1);
				//System.out.println(spots);
				for(int x=0;x<pot.length;x++)
					if(!spots.contains(x))
						retopotato+=pot[x]+(x!=pot.length-1?"_":"");
				if(retopotato.charAt(retopotato.length()-1)=='_')
					retopotato+="Not Graded Yet...";
				oppa.add(retopotato);
			}
			reto.put(key,oppa);
		}
		return reto;
	}

	//sweepTags(K,T) Searches 'T' for a certain tag 'K' and marks it with a "~", REQUIRED for getClasswork to work

	private static String sweepTags(String k,String t){
		String ret = "";
		String consta = "Date Due  Date Assigned  Assignment  Category  Score  Total Points  Weight  Weighted Score  Weighted Total Points  Percentage";
		if(t.contains(k))
			ret+="~";
		char[] tc = t.toCharArray();
		boolean tagged = false;
		for (char aTc : tc) {
			if (aTc == '<')
				tagged = true;
			if (aTc == '>')
				tagged = false;
			if (!tagged)
				ret += "" + (aTc != '*' ? aTc : "");
		}
		ret=ret.trim()
				.replaceAll(">"," ")
				.replaceAll("&nbsp","")
				.replaceAll(";","")
				.replaceAll("&amp","&")
				.replaceAll(consta,"")
				.replaceAll("\\s"," ")
				.replaceAll("#","+")
				.trim()
				.replaceAll("    ","____")
				.replaceAll("   ","___")
				.replaceAll("  ","__")
				.replaceAll("__","_")
				.replaceAll("__","_")
				.replaceAll("__","_")
				.replaceAll("__","_")
				.replaceAll("__","_")
				.replaceAll("Total","\nTotal")
				.replaceAll("Course","\nCourse")
				.replaceAll("Categories","\nCategories")
				.replaceAll("Show All Averages","")
				.replaceAll("\n","@")
				.trim();
		return ret;
	}

	//sweepTags(Pass) just cleans out the tags, REQUIRED for getClassOrder to work

	private static String sweepTags(String pass){
		return sweepTags("Ω",pass);
	}

	//filterClasses(String[] q) looks for the classCodes and is required for getClasswork(String s) to work

	private static ArrayList<String> filterClasses(String[] q){
		ArrayList<String> ret = new ArrayList<>();
		for(String r:ret)
			if(r.contains("<option value=\"1")&&r.length()>30)
				ret.add(r.substring(r.indexOf(">") + 1, r.lastIndexOf("<")).trim());
		return ret;
	}

	//getClassOrder(String s) you pass in 'S' which is the URL to locate the overview page and it
	//returns an ArrayList<String> of the classes which filters out lunches and checks for duplicates

	public final static ArrayList<String> getClassOrder(String s,boolean isFilePathURL){
		ArrayList<String> ret = new ArrayList<>();
		try{
			String work ="";
			if(isFilePathURL){
				URL uri = new URL(s);
				Scanner scan = new Scanner(uri.openStream());
				while(scan.hasNextLine())
					work+=scan.nextLine()+"\n";
			}else work=s;
			String[] not = work.split("\n");
			for(int x=0;x<not.length;x++)
				not[x]=sweepTags(not[x]);
			for(String t:not)
				if(t.contains("#-")&&!t.contains("9999")&&!ret.contains(t))
					ret.add(t);
			for(int x=0;x<ret.size();x++)
				ret.set(x, ret.get(x).substring(1,6));

		}catch(Exception e){e.printStackTrace();}
		return ret;
	}

	//pass 'S' which is the Summary URL and pass 'F' which is the Assignment URL and it'll return the class names in order

	public final static ArrayList<String> getClassNamesInOrder(String s,boolean isFilePathURL){
		ArrayList<String> ret = new ArrayList<>();
		try{
			String work ="";
			if(isFilePathURL){
				URL uri = new URL(s);
				Scanner scan = new Scanner(uri.openStream());
				while(scan.hasNextLine())
					work+=scan.nextLine()+"\n";
			}else work=s;
			String[] zoop = work.split("\n");
			for(String sz:zoop){
				if(sz.contains("ClassPopUp.aspx")) {
					String temp = sweepTags(sz);
					if (!ret.contains(temp))
						ret.add(temp);
				}
			}
		}catch(Exception e){e.printStackTrace();}
		ret.removeAll(Arrays.asList("Lunch"));
		return ret;
	}

}


class Course{
    private String teacher;
    private String classNum;
    private String className;
    private static int longestCourse=Integer.MIN_VALUE;
    Course(String t,String cnu,String cna){
        teacher=cleanTeacher(t);
        classNum=cnu;
        className=cna;
        if(className.length()>longestCourse)
            longestCourse=className.length();
    }
    public String getTeacher(){return teacher;}

    private String cleanTeacher(String t){
        String ret = "";
        char[] to = t.toCharArray();
        for(int x=0;x<to.length;x++)
            if(to[x]=='.')
                to[x]=' ';
        for(int x=0;x<to.length;x++)
            if(x==0)
                to[x]=Character.toUpperCase(to[x]);
            else if(to[x-1]==' ')
                to[x]=Character.toUpperCase(to[x]);
        for(char c:to)
            ret+=""+c;
        return ret;
    }

    public String getClassNum(){return classNum;}

    public String getClassName(){return className;}

    public String toString(){return classNum+" | "+String.format("%"+longestCourse+"s",className)+" | "+teacher;}

}