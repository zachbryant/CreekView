package burntapples.github.io.creekview.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.widget.Toast;

import burntapples.github.io.creekview.AES;
import burntapples.github.io.creekview.R;
import burntapples.github.io.creekview.Utils;

public class SplashActivity extends Activity {
    private static final int SPLASH_DISPLAY_TIME = 700; // splash screen delay time
    private Utils utils;
    private AES encryptor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_creek_view_splash_screen);
        utils=new Utils(this);
        encryptor= new AES(this.getApplicationContext());
        new Handler().postDelayed(new Runnable() {
            public void run() {

                Intent intent= new Intent(getApplicationContext(), HomeViewLoginActivity.class);

                if(encryptor.land("student_password")!=null && encryptor.land("student_id")!=null) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    int login = utils.refresh();
                    switch (login) {
                        case 200:
                            intent = new Intent(getApplicationContext(), ViewSwipeActivity.class);
                            break;
                        case -1:
                            Toast.makeText(utils.getContext(),"Your username or password is incorrect.",Toast.LENGTH_SHORT).show();
                            intent = new Intent(getApplicationContext(), HomeViewLoginActivity.class);
                            break;
                        case -2:
                            Toast.makeText(utils.getContext(),"Please check your internet connection and try again.",Toast.LENGTH_SHORT).show();
                            intent = new Intent(getApplicationContext(), HomeViewLoginActivity.class);
							break;
						default:
							Toast.makeText(utils.getContext(),"HomeAccess is unavailable. Try again later.",Toast.LENGTH_SHORT).show();
							intent = new Intent(getApplicationContext(), HomeViewLoginActivity.class);
                    }
                }

                startActivity(intent);
                finish();
                // transition from splash to main menu
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        }, SPLASH_DISPLAY_TIME);
    }
}
