package burntapples.github.io.creekview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Group {

	public String string;
	public final List<Assignment> children = new ArrayList<>();

	public Group(String string) {
		this.string = string;
	}
	public void sort(){Collections.sort(children);}

}