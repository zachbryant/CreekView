package burntapples.github.io.creekview;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.commons.lang.WordUtils;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import burntapples.github.io.creekview.parser.GradeParser;
import burntapples.github.io.creekview.parser.OverviewParser;

public class Utils {
    private Activity activity;
    private AES encryptor;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	StringBuilder data_doc = new StringBuilder();
    public Utils(Activity activity){
        this.activity=activity;
        encryptor = new AES(activity.getApplicationContext());
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    public int login(){
        String loginsite = "https://home-access.cfisd.net/HomeAccess/Account/LogOn";
        String overview = "https://home-access.cfisd.net/HomeAccess/Home/WeekView";
        String grades = "https://home-access.cfisd.net/HomeAccess/Content/Student/Assignments.aspx";
        String stud_ID = encryptor.land("student_id");
        String stud_pass = encryptor.land("student_password");
        String jsonData = String.format("{'Database':10,'LogOnDetails.UserName':'%s','LogOnDetails.Password':'%s'}",stud_ID,stud_pass);

        OkHttpClient okclient;
        int code;

        if(!isIDValid(stud_ID) || !isPasswordValid(stud_pass))
            return -1;
        try{
            okclient = new OkHttpClient();
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
            okclient.setCookieHandler(cookieManager);
            code=post(loginsite,jsonData,okclient);
            condition:if(code==200) {
                String gradeContent = getPageContent(grades,okclient);
                String overviewContent = getPageContent(overview,okclient);

                if(overviewContent.contains("Login")) {
                    code = -1;
                    if(overviewContent.contains("unavailable"))
                        code=-3;
                    break condition;
                }
				Gson gson = new Gson();
                Grade.setContext(activity);
                ArrayList<String> classes = OverviewParser.getClassNamesInOrder(overviewContent, false);
                ArrayList<String> teachers = OverviewParser.getTeachers(overviewContent, false);
                addClasses(classes, teachers);
                Set<ArrayList<String>> cleanData = new HashSet<>(GradeParser.getClasswork(gradeContent).values());
                for(ArrayList<String> data:cleanData)
					addAssignments(data);

            }
        }catch(Exception e){
            e.printStackTrace();
            return -2;
        }

        return code;
    }

    public void addClasses(ArrayList<String> classes, ArrayList<String> teachers)throws NullPointerException{
        System.out.println(classes);
        Class[] list = Class.values();
        int index=0;
		Gson gson = new Gson();

        for(int x=0;x<classes.size();x++){
            Class c = list[index++];
            String className=classes.get(x);
            if(!className.equalsIgnoreCase("Lunch")) {
				String teacher = WordUtils.capitalize(teachers.get(x).replace("[\\.]", " "));
				Grade.setTeacher(c, teacher);
				Grade.setClass(c, className);
				data_doc.append(gson.toJson(new Object[]{c, className, teacher}));
				data_doc.append("\n");
			}
        }
    }

    public void addAssignments(ArrayList<String> byLine){
        Class c = null;
		Gson gson = new Gson();

        for(String line:byLine){
            line=line.toLowerCase().trim();
            System.out.println(line);
            String[] parts = line.split("_");
            if(parts[0].matches("\\d\\d/\\d\\d/\\d\\d\\d\\d")){
                String dateDue = parts[0];
                String dateAssigned = parts[1];
                String assignmentName = parts[2];
                String assignmentType = parts[3];
                String grade="";

                if(!dateAssigned.matches("\\d\\d/\\d\\d/\\d\\d\\d\\d")){
                    dateAssigned="";
                    assignmentName=parts[1].replaceAll("&#39","'");
                    assignmentType=parts[2];
                    if(parts.length==4)
                        grade = parts[3].replaceAll("[%]","");
                }
                else
                    if(parts.length==5)
                        grade = parts[4].replaceAll("[%]","");
				Assignment a = new Assignment(assignmentName,grade,assignmentType,dateAssigned, dateDue);
				data_doc.append(gson.toJson(new Object[]{c,a}));
                Grade.notify(c,a);
                Grade.addAssignment(c,a);
            }
            else{
                if(parts[0].equals("~")){
                    String[] classDetails=parts[3].split(" ");
					String className = parts[2];
					String average = classDetails[classDetails.length-1];
                    c=Grade.getClassEnumByCourseName(className);
					Grade.setAverage(c,Double.valueOf(average.substring(0,average.length()-1)));
                }
            }
        }
    }

    protected String getPageContent(String url, OkHttpClient client)throws IOException {
        Request request = new Request.Builder()
                .addHeader("accept-language", "en-US,en;q=0.8")
                .addHeader("referer", "https://home-access.cfisd.net/HomeAccess/Account/LogOn")
                .addHeader("DNT", "1")
                .addHeader("connection", "keep-alive")
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("cache-control", "max-age=0")
                .addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
                .addHeader("user-agent", "Mozilla/5.0")
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    private int post(String url, String json, OkHttpClient client) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .addHeader("Accept-Language", "en-US,en;q=0.8")
                .addHeader("Host", "home-access.cfisd.net")
                .addHeader("Origin", "https://home-access.cfisd.net")
                .addHeader("DNT", "1")
                .addHeader("User-Agent", "Mozilla/5.0")
                .addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
                .addHeader("cache-control", "max-age=0")
                .addHeader("Connection", "keep-alive")
                .addHeader("Referer", "https://home-access.cfisd.net/HomeAccess/Account/LogOn?ReturnUrl=%2fHomeAccess%2f")
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.code();
    }

    public ArrayList<ArrayList<Assignment>> getOverviewCardDescriptions(){
        Set<Map.Entry<Class,ArrayList<Assignment>>> assignments = Grade.getAssignmentsMap().entrySet();
        ArrayList<Assignment> futureList= new ArrayList<>();
        ArrayList<Assignment> recentList= new ArrayList<>();

        for(Map.Entry<Class,ArrayList<Assignment>> e:assignments){
            ArrayList<Assignment> assignment = e.getValue();
            for(Assignment a:assignment){
                if(inRange(a.getDateAssigned(), 7, false)){
                    recentList.add(a);
                    //System.out.println(a);
                    continue;
                }
                if(inRange(a.getDateDue(), 7, true))
                    futureList.add(a);
            }
        }
        ArrayList<ArrayList<Assignment>> sendList= new ArrayList<>();
        sendList.add(futureList);
        sendList.add(recentList);
        return sendList;
    }

    public Context getContext(){return activity.getApplicationContext();}

    public void storeLoginInfo(){
        encryptor.jet("Student_ID", ((TextView) activity.findViewById(R.id.student_id)).getText().toString());
        encryptor.jet("Student_PASSWORD", ((EditText) activity.findViewById(R.id.password)).getText().toString());
    }

    protected boolean inRange(String date, int range,boolean future){
        if(date.length()!=10)
            return false;
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_YEAR);
        int year = calendar.get(Calendar.YEAR);
        String[] cleanDate = date.split("[/]");
        Calendar current = Calendar.getInstance();
        //System.out.println(Arrays.toString(cleanDate));
        current.set(new Integer(cleanDate[2]),new Integer(cleanDate[0])-1,new Integer(cleanDate[1]));
        int assigned =current.get(Calendar.DAY_OF_YEAR);
        //System.out.println(future+" "+day+" - "+assigned);
        if(cleanDate.length!=3)
            return false;
        if(Integer.valueOf(cleanDate[2])==year)
            if(future){
                if((day-range)<=assigned && assigned<day)
                    return true;
            }
        else{
            if(day>=(assigned-range) && assigned>=day)
                return true;
            }
        return false;
    }

    public boolean isIDValid(String ID) {
        try{
            int i = Integer.parseInt(ID.substring(1));
        }catch(Exception e){return false;}
        return ID.length()==7&& ID.toUpperCase().charAt(0)=='S';
    }

    public boolean isPasswordValid(String password) {
        return password.length() > 0;
    }

    public static int getColor(String g){
        //int grade = Integer.valueOf(g);
        return 0x330AE2;
    }

	public int refresh(){
		long lastRefresh = Long.valueOf(encryptor.land("REFRESH_TIME"));
		long timeInMinutes = ((System.currentTimeMillis()/1000)/60);
		int code=200;
		if(timeInMinutes-lastRefresh>=15)
			code=login();
		encryptor.jet("REFRESH_TIME",""+timeInMinutes);
		return code;
	}
}

