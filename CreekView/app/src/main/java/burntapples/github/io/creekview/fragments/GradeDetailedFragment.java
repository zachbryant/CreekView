package burntapples.github.io.creekview.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import burntapples.github.io.creekview.Class;
import burntapples.github.io.creekview.Grade;
import burntapples.github.io.creekview.Group;
import burntapples.github.io.creekview.R;
import burntapples.github.io.creekview.Utils;
import burntapples.github.io.creekview.activities.ViewSwipeActivity;
import burntapples.github.io.creekview.adapter.ExpandableCardListAdapter;

public class GradeDetailedFragment extends Fragment {
	// more efficient than HashMap for mapping integers to objects
	SparseArray<Group> groups = new SparseArray<>();
	Utils utils = ViewSwipeActivity.utils;

	public GradeDetailedFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_grade_extended,container,false);
		createData();
		ExpandableListView listView = (ExpandableListView) rootView.findViewById(R.id.list_grades);
		ExpandableCardListAdapter adapter = new ExpandableCardListAdapter(inflater, groups);
		listView.setAdapter(adapter);
		return rootView;
	}
	public void createData() {
		Group first = new Group(String.format("%s%-4s",Grade.getClass(Class.FIRST),Grade
				.getAverage
				(Class.FIRST)));
		Group second = new Group(String.format("%s%-4s",Grade.getClass(Class.SECOND),Grade
				.getAverage(Class.SECOND)));
		Group third = new Group(String.format("%s%-4s",Grade.getClass(Class.THIRD),Grade
				.getAverage(Class.THIRD)));
		Group fourth = new Group(String.format("%s%-4s",Grade.getClass(Class.FOURTH),Grade
				.getAverage(Class.FOURTH)));
		Group fifth = new Group(String.format("%s%-4s",Grade.getClass(Class.FIFTH),Grade
				.getAverage(Class.FIFTH)));
		Group sixth = new Group(String.format("%s%-4s",Grade.getClass(Class.SIXTH),Grade
				.getAverage(Class.SIXTH)));
		Group seventh = new Group(String.format("%s%-4s",Grade.getClass(Class.SEVENTH),Grade
				.getAverage(Class.SEVENTH)));

		first.children.addAll(Grade.getAssignments(Class.FIRST));
		second.children.addAll(Grade.getAssignments(Class.SECOND));
		third.children.addAll(Grade.getAssignments(Class.THIRD));
		fourth.children.addAll(Grade.getAssignments(Class.FOURTH));
		fifth.children.addAll(Grade.getAssignments(Class.FIFTH));
		sixth.children.addAll(Grade.getAssignments(Class.SIXTH));
		seventh.children.addAll(Grade.getAssignments(Class.SEVENTH));

		groups.append(0,first);
		groups.append(1,second);
		groups.append(2,third);
		groups.append(3,fourth);
		groups.append(4,fifth);
		groups.append(5,sixth);
		groups.append(6,seventh);
	}
}
